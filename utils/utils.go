package utils

import (
	"encoding/json"
	"log"
)

func ToJSON(data interface{}) string {
	r, err := json.Marshal(data)
	if err != nil {
		log.Printf("Error parsing json: %v", err)
	}
	return string(r)
}

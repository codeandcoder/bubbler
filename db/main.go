package db

import (
	gonorm "github.com/marpaia/GonormCypher"
)

var g *gonorm.Gonorm

// Init initializes the database
func Init() {
	g = gonorm.New("http://localhost", 7474)
}

func GetG() *gonorm.Gonorm {
	return g
}

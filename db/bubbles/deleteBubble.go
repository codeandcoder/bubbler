package bubbles

import (
	"bubbler/db"
	"time"
)

const (
	deleteBubbleQuery = `MATCH (u:User{Uuid:{deleterID}})
					  MATCH (b:Bubble {Uuid:{bubbleID}})
					  CREATE (u)-[:DELETED{At:{timestamp}}]->(b)
					  RETURN b.Uuid`
)

// DeleteBubble sets a bubble as deleted
func DeleteBubble(bubbleID string, deleterID string) error {
	timestamp := time.Now().Unix()

	result := db.GetG().Cypher(deleteBubbleQuery).On(map[string]interface{}{
		"deleterID": deleterID,
		"bubbleID":  bubbleID,
		"timestamp": timestamp,
	}).Execute()

	return result.Error
}

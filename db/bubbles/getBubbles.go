package bubbles

import (
	"bubbler/db"

	"github.com/mitchellh/mapstructure"
)

const (
	getBubbleQuery = `MATCH (u:User)-[:POSTS]-(b:Bubble) WHERE b.Uuid = {bubbleID}
					  RETURN properties(u), properties(b)`
	getBubblesQuery = `MATCH (u:User)
					  WHERE u.Uuid = {userID}
					  MATCH (o:User)-[:POSTS]->(b:Bubble)
					  WHERE NOT (b)<-[:DELETED]-()
					  AND NOT (u)-[:BLOCKS]->(o)
					  RETURN properties(o), properties(b)`
)

type GetBubbleResult struct {
	OwnerData  `json:"OwnerData"`
	BubbleData `json:"BubbleData"`
}

type OwnerData struct {
	Uuid string `json:"Uuid"`
	Name string `json:"Name"`
}

type BubbleData struct {
	Uuid    string `json:"Uuid"`
	Content string `json:"Content"`
}

// GetBubble retrieves a bubble
func GetBubble(bubbleID string) GetBubbleResult {
	result := db.GetG().Cypher(getBubbleQuery).On(map[string]interface{}{
		"bubbleID": bubbleID,
	}).Execute()
	return parseGetBubble(result.Data[0].([]interface{}))
}

// GetBubbles retrieves all bubbles
func GetBubbles(userID string) []GetBubbleResult {
	result := db.GetG().Cypher(getBubblesQuery).On(map[string]interface{}{
		"userID": userID,
	}).Execute()
	getBubblesResults := make([]GetBubbleResult, 0)
	for _, r := range result.Data {
		getBubblesResults = append(getBubblesResults, parseGetBubble(r.([]interface{})))
	}
	return getBubblesResults
}

func parseGetBubble(data []interface{}) GetBubbleResult {
	var getBubbleResult GetBubbleResult
	var ownerData OwnerData
	mapstructure.Decode(data[0].(map[string]interface{}), &ownerData)
	getBubbleResult.OwnerData = ownerData
	var bubbleData BubbleData
	mapstructure.Decode(data[1].(map[string]interface{}), &bubbleData)
	getBubbleResult.BubbleData = bubbleData
	return getBubbleResult
}

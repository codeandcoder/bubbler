package bubbles

import (
	"bubbler/db"
	"time"

	"github.com/google/uuid"
)

const (
	newBubbleQuery = `MATCH (u:User{Uuid:{ownerID}})
					  CREATE (u)-[:POSTS{At:{timestamp}}]->(b:Bubble {Uuid:{uuid}, Content:{content}})
					  RETURN b.Uuid`
)

// NewBubble creates a new bubble node
func NewBubble(ownerID string, content string) error {
	id, _ := uuid.NewRandom()
	timestamp := time.Now().Unix()

	result := db.GetG().Cypher(newBubbleQuery).On(map[string]interface{}{
		"ownerID":   ownerID,
		"uuid":      id,
		"content":   content,
		"timestamp": timestamp,
	}).Execute()

	return result.Error
}

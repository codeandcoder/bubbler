package bubbles

import (
	"bubbler/db"
	"time"

	"github.com/google/uuid"
)

const (
	popBubbleQuery = `MATCH (u:User{Uuid:{popperID}})
					  MATCH (b:Bubble {Uuid:{bubbleID}})
					  CREATE (u)-[:POPPED{Intensity:{intensity}, At:{timestamp}}]->(b)
					  RETURN b.Uuid`
	commentBubbleQuery = `MATCH (u:User{Uuid:{ownerID}})
					  MATCH (b:Bubble {Uuid:{referencedBubbleID}})
					  CREATE (u)-[:POSTS{At:{timestamp}}]->(nb:Bubble {Uuid:{uuid}, Content:{content}})-[:COMMENTS]->(b)
					  RETURN nb.Uuid`
	swipeBubbleQuery = `MATCH (u:User{Uuid:{swiperID}})
					  MATCH (b:Bubble {Uuid:{bubbleID}})
					  CREATE (u)-[:SWIPES{At:{timestamp}}]->(b)
					  RETURN b.Uuid`
)

// PopBubble creates a relation between the bubble and the user popping the bubble with a certain intensity
func PopBubble(bubbleID string, popperID string, intensity float64) error {
	timestamp := time.Now().Unix()

	result := db.GetG().Cypher(popBubbleQuery).On(map[string]interface{}{
		"popperID":  popperID,
		"bubbleID":  bubbleID,
		"intensity": intensity,
		"timestamp": timestamp,
	}).Execute()

	return result.Error
}

// CommentBubble creates a new bubble node commenting another bubble node
func CommentBubble(referencedBubbleID string, ownerID string, content string) error {
	id, _ := uuid.NewRandom()
	timestamp := time.Now().Unix()

	result := db.GetG().Cypher(commentBubbleQuery).On(map[string]interface{}{
		"ownerID":            ownerID,
		"referencedBubbleID": referencedBubbleID,
		"uuid":               id,
		"content":            content,
		"timestamp":          timestamp,
	}).Execute()

	return result.Error
}

// SwipeBubble creates a relation between the bubble and the user swiping the bubble
func SwipeBubble(bubbleID string, swiperID string) error {
	timestamp := time.Now().Unix()

	result := db.GetG().Cypher(swipeBubbleQuery).On(map[string]interface{}{
		"swiperID":  swiperID,
		"bubbleID":  bubbleID,
		"timestamp": timestamp,
	}).Execute()

	return result.Error
}

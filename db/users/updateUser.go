package users

import (
	"bubbler/db"
	"time"
)

const (
	followUserQuery = `MATCH (u:User) WHERE u.Uuid = {userID}
					   MATCH (f:User) WHERE f.Uuid = {followedID}
					   CREATE (u)-[r:FOLLOWS{At:{timestamp}}]->(f)
					   RETURN r`
	blockUserQuery = `MATCH (u:User) WHERE u.Uuid = {userID}
					   MATCH (f:User) WHERE f.Uuid = {blockedID}
					   CREATE (u)-[r:BLOCKS{At:{timestamp}}]->(f)
					   RETURN r`
)

// FollowUser follows a user
func FollowUser(followedID string, userID string) error {
	timestamp := time.Now().Unix()
	result := db.GetG().Cypher(followUserQuery).On(map[string]interface{}{
		"followedID": followedID,
		"userID":     userID,
		"timestamp":  timestamp,
	}).Execute()
	return result.Error
}

// BlockUser blocks a user
func BlockUser(blockedID string, userID string) error {
	timestamp := time.Now().Unix()
	result := db.GetG().Cypher(blockUserQuery).On(map[string]interface{}{
		"blockedID": blockedID,
		"userID":    userID,
		"timestamp": timestamp,
	}).Execute()
	return result.Error
}

package bubbler

import (
	"bubbler/bubbler/bubbles"
	"bubbler/bubbler/users"
	"bubbler/db"
	"log"
)

var work = make(chan interface{}, 1000)

// Init creates the workers
func Init(workerCount int) {
	db.Init()
	for i := 0; i < workerCount; i++ {
		go bubbler(i)
	}
	log.Printf("Bubbler initialized with %v workers.", workerCount)
}

// PublishWork inserts work into the work channel
func PublishWork(w interface{}) {
	work <- w
}

func bubbler(wid int) {
	for {
		w := <-work
		switch w.(type) {
		case bubbles.NewBubbleMessage:
			bubbles.NewBubble(wid, w.(bubbles.NewBubbleMessage))
		case bubbles.CommentedBubbleMessage:
			bubbles.CommentBubble(wid, w.(bubbles.CommentedBubbleMessage))
		case bubbles.PoppedBubbleMessage:
			bubbles.PopBubble(wid, w.(bubbles.PoppedBubbleMessage))
		case bubbles.SwipedBubbleMessage:
			bubbles.SwipeBubble(wid, w.(bubbles.SwipedBubbleMessage))
		case bubbles.GetBubbleMessage:
			bubbles.GetBubble(wid, w.(bubbles.GetBubbleMessage))
		case bubbles.GetBubblesMessage:
			bubbles.GetBubbles(wid, w.(bubbles.GetBubblesMessage))
		case bubbles.DeletedBubbleMessage:
			bubbles.DeleteBubble(wid, w.(bubbles.DeletedBubbleMessage))
		case users.FollowedUserMessage:
			users.FollowUser(wid, w.(users.FollowedUserMessage))
		case users.BlockedUserMessage:
			users.BlockUser(wid, w.(users.BlockedUserMessage))
		}
	}
}

package bubbles

import (
	dbBubbles "bubbler/db/bubbles"
	"log"

	"github.com/graarh/golang-socketio"
)

// NewBubbleMessage holds a new bubble data
type NewBubbleMessage struct {
	SocketsServer *gosocketio.Server
	OwnerSocket   *gosocketio.Channel
	OwnerID       string
	Content       string
}

func NewBubble(wid int, nb NewBubbleMessage) {
	log.Printf("Worker %v, New Bubble: %v\n", wid, nb)
	err := dbBubbles.NewBubble(nb.OwnerID, nb.Content)
	if err != nil {
		log.Printf("Error trying to create new bubble: %v", err)
	}
}

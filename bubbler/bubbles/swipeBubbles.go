package bubbles

import (
	dbBubbles "bubbler/db/bubbles"
	"log"

	"github.com/graarh/golang-socketio"
)

// SwipedBubbleMessage holds a bubble swiped data
type SwipedBubbleMessage struct {
	SocketsServer *gosocketio.Server
	SwiperSocket  *gosocketio.Channel
	BubbleID      string
	SwiperID      string
}

func SwipeBubble(wid int, sb SwipedBubbleMessage) {
	log.Printf("Worker %v, Swiped Bubble: %v\n", wid, sb)
	err := dbBubbles.SwipeBubble(sb.BubbleID, sb.SwiperID)
	if err != nil {
		log.Printf("Error trying to swipe a bubble: %v", err)
	}
}

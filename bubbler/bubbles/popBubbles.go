package bubbles

import (
	dbBubbles "bubbler/db/bubbles"
	"log"

	"github.com/graarh/golang-socketio"
)

// PoppedBubbleMessage holds a bubble popping data
type PoppedBubbleMessage struct {
	SocketsServer *gosocketio.Server
	OwnerSocket   *gosocketio.Channel
	PopperSocket  *gosocketio.Channel
	BubbleID      string
	PopperID      string
	Intensity     float64
}

func PopBubble(wid int, pb PoppedBubbleMessage) {
	log.Printf("Worker %v, Pop Bubble: %v\n", wid, pb)
	err := dbBubbles.PopBubble(pb.BubbleID, pb.PopperID, pb.Intensity)
	if err != nil {
		log.Printf("Error trying to pop a bubble: %v", err)
	}
}

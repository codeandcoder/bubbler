package bubbles

import (
	dbBubbles "bubbler/db/bubbles"
	"bubbler/utils"
	"log"

	"github.com/graarh/golang-socketio"
)

// GetBubblesMessage holds the sockets data
type GetBubblesMessage struct {
	SocketsServer *gosocketio.Server
	OwnerSocket   *gosocketio.Channel
	UserID        string
}

func GetBubbles(wid int, gb GetBubblesMessage) {
	log.Printf("Worker %v, Get Bubbles: %v\n", wid, gb)
	result := dbBubbles.GetBubbles(gb.UserID)
	gb.OwnerSocket.Emit("bubbles:get", utils.ToJSON(result))
}

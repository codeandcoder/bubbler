package bubbles

import (
	dbBubbles "bubbler/db/bubbles"
	"bubbler/utils"
	"log"

	"github.com/graarh/golang-socketio"
)

// GetBubbleMessage holds the sockets data
type GetBubbleMessage struct {
	SocketsServer *gosocketio.Server
	OwnerSocket   *gosocketio.Channel
	BubbleID      string
}

func GetBubble(wid int, gb GetBubbleMessage) {
	log.Printf("Worker %v, Get Bubble: %v\n", wid, gb)
	result := dbBubbles.GetBubble(gb.BubbleID)
	gb.OwnerSocket.Emit("bubble:get", utils.ToJSON(result))
}

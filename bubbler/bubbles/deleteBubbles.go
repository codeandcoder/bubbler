package bubbles

import (
	dbBubbles "bubbler/db/bubbles"
	"log"

	"github.com/graarh/golang-socketio"
)

// DeletedBubbleMessage holds a bubble deleting data
type DeletedBubbleMessage struct {
	SocketsServer *gosocketio.Server
	OwnerSocket   *gosocketio.Channel
	DeleterSocket *gosocketio.Channel
	BubbleID      string
	DeleterID     string
}

func DeleteBubble(wid int, delb DeletedBubbleMessage) {
	log.Printf("Worker %v, Delete Bubble: %v\n", wid, delb)
	bubbleInfo := dbBubbles.GetBubble(delb.BubbleID)
	if bubbleInfo.OwnerData.Uuid != delb.DeleterID {
		log.Printf("Error trying to delete a bubble: Deleter is not the Owner")
	} else {
		err := dbBubbles.DeleteBubble(delb.BubbleID, delb.DeleterID)
		if err != nil {
			log.Printf("Error trying to delete a bubble: %v", err)
		}
	}
}

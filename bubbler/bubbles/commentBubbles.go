package bubbles

import (
	dbBubbles "bubbler/db/bubbles"
	"log"

	"github.com/graarh/golang-socketio"
)

// CommentedBubbleMessage holds a new bubble data commenting on another
type CommentedBubbleMessage struct {
	SocketsServer      *gosocketio.Server
	OwnerSocket        *gosocketio.Channel
	CommenterSocket    *gosocketio.Channel
	ReferencedBubbleID string
	OwnerID            string
	Content            string
}

func CommentBubble(wid int, cb CommentedBubbleMessage) {
	log.Printf("Worker %v, Commented Bubble: %v\n", wid, cb)
	err := dbBubbles.CommentBubble(cb.ReferencedBubbleID, cb.OwnerID, cb.Content)
	if err != nil {
		log.Printf("Error trying to comment a bubble: %v", err)
	}
}

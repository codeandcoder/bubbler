package users

import (
	dbUsers "bubbler/db/users"
	"log"

	"github.com/graarh/golang-socketio"
)

// FollowUserMessage holds a user following data
type FollowedUserMessage struct {
	SocketsServer  *gosocketio.Server
	OwnerSocket    *gosocketio.Channel
	FollowedSocket *gosocketio.Channel
	FollowedID     string
	UserID         string
}

func FollowUser(wid int, fu FollowedUserMessage) {
	log.Printf("Worker %v, User Followed: %v\n", wid, fu)
	if fu.FollowedID == fu.UserID {
		log.Printf("Error trying to follow a user: FollowerID and UserID are the same.")
	} else {
		err := dbUsers.FollowUser(fu.FollowedID, fu.UserID)
		if err != nil {
			log.Printf("Error trying to follow a user: %v", err)
		}
	}
}

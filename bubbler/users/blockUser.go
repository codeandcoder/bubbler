package users

import (
	dbUsers "bubbler/db/users"
	"log"

	"github.com/graarh/golang-socketio"
)

// BlockUserMessage holds a user blocking data
type BlockedUserMessage struct {
	SocketsServer *gosocketio.Server
	OwnerSocket   *gosocketio.Channel
	BlockedID     string
	UserID        string
}

func BlockUser(wid int, bu BlockedUserMessage) {
	log.Printf("Worker %v, User Blocked: %v\n", wid, bu)
	if bu.BlockedID == bu.UserID {
		log.Printf("Error trying to block a user: BlockedID and UserID are the same.")
	} else {
		err := dbUsers.BlockUser(bu.BlockedID, bu.UserID)
		if err != nil {
			log.Printf("Error trying to block a user: %v", err)
		}
	}
}

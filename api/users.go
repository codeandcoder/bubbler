package api

import (
	"bubbler/bubbler"
	bubblerUsers "bubbler/bubbler/users"

	"github.com/graarh/golang-socketio"
)

// FollowUser signals the creation of a new following relation
func FollowUser(socket *gosocketio.Channel, fu bubblerUsers.FollowedUserMessage) string {
	fu.SocketsServer = server
	fu.OwnerSocket = socket
	// TODO: Find and assign the Socket of the followed user
	bubbler.PublishWork(fu)
	return "{Status:'OK'}"
}

// BlockUser signals the creation of a new user blocking relation
func BlockUser(socket *gosocketio.Channel, bu bubblerUsers.BlockedUserMessage) string {
	bu.SocketsServer = server
	bu.OwnerSocket = socket
	bubbler.PublishWork(bu)
	return "{Status:'OK'}"
}

package api

import (
	"bubbler/bubbler"
	bubblerBubbles "bubbler/bubbler/bubbles"

	"github.com/graarh/golang-socketio"
)

// NewBubble signals the creation of a new Bubble
func NewBubble(socket *gosocketio.Channel, nb bubblerBubbles.NewBubbleMessage) string {
	nb.SocketsServer = server
	nb.OwnerSocket = socket
	bubbler.PublishWork(nb)
	return "{Status:'OK'}"
}

// PopBubble signals the popping of a bubble
func PopBubble(socket *gosocketio.Channel, pb bubblerBubbles.PoppedBubbleMessage) string {
	pb.SocketsServer = server
	pb.PopperSocket = socket
	// TODO: Find and assign the Socket of the bubble owner
	bubbler.PublishWork(pb)
	return "{Status:'OK'}"
}

// CommentBubble signals the creation of a bubble commenting on another
func CommentBubble(socket *gosocketio.Channel, cb bubblerBubbles.CommentedBubbleMessage) string {
	cb.SocketsServer = server
	cb.CommenterSocket = socket
	// TODO: Find and assign the Socket of the bubble owner
	bubbler.PublishWork(cb)
	return "{Status:'OK'}"
}

// SwipeBubble signals the swiping of a bubble
func SwipeBubble(socket *gosocketio.Channel, sb bubblerBubbles.SwipedBubbleMessage) string {
	sb.SocketsServer = server
	sb.SwiperSocket = socket
	bubbler.PublishWork(sb)
	return "{Status:'OK'}"
}

// GetBubbles signals the retrieval of bubbles
func GetBubbles(socket *gosocketio.Channel, gb bubblerBubbles.GetBubblesMessage) string {
	gb.SocketsServer = server
	gb.OwnerSocket = socket
	bubbler.PublishWork(gb)
	return "{Status:'OK'}"
}

// GetBubble signals the retrieval of a bubble
func GetBubble(socket *gosocketio.Channel, gb bubblerBubbles.GetBubbleMessage) string {
	gb.SocketsServer = server
	gb.OwnerSocket = socket
	bubbler.PublishWork(gb)
	return "{Status:'OK'}"
}

// DeleteBubble signals the deletion of a bubble
func DeleteBubble(socket *gosocketio.Channel, delb bubblerBubbles.DeletedBubbleMessage) string {
	delb.SocketsServer = server
	delb.DeleterSocket = socket
	// TODO: Find and assign the Socket of the bubble owner
	bubbler.PublishWork(delb)
	return "{Status:'OK'}"
}

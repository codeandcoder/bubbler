package api

import (
	"bubbler/bubbler"
	"log"
	"net/http"
	"runtime"

	"github.com/graarh/golang-socketio"
	"github.com/graarh/golang-socketio/transport"
)

var server *gosocketio.Server

// Start initializes the API
func Start() {
	bubbler.Init(runtime.NumCPU() - 1)
	server = gosocketio.NewServer(transport.GetDefaultWebsocketTransport())
	log.Print("SocketIO Server initialized successfully!")

	server.On(gosocketio.OnConnection, connectUser)
	server.On(gosocketio.OnDisconnection, disconnectUser)
	server.On("bubble:new", NewBubble)
	server.On("bubble:pop", PopBubble)
	server.On("bubble:comment", CommentBubble)
	server.On("bubble:swipe", SwipeBubble)
	server.On("bubble:get", GetBubble)
	server.On("bubble:delete", DeleteBubble)
	server.On("bubbles:get", GetBubbles)
	server.On("user:follow", FollowUser)
	server.On("user:block", BlockUser)

	http.Handle("/", server)
	log.Println("Serving at localhost:2222...")
	log.Fatal(http.ListenAndServe(":2222", nil))
}

func connectUser(c *gosocketio.Channel) string {
	log.Println("User connected")
	return "OK"
}

func disconnectUser(c *gosocketio.Channel) string {
	log.Println("User disconnected")
	return "OK"
}
